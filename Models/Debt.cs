using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sistema.Models {

    public class Debt {

        [Key]
        public int id { get; set; }
        public Customer customer { get; set; }
        public DateTime dueDate { get; set; }
        public int installments { get; set; }
        public double value { get; set; }
    }
}