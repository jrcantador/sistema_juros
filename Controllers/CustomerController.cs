using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using db.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sistema.Models;

namespace sistema.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class CustomerController : Controller {

        private readonly dbContext _context;

        public CustomerController (dbContext context) {
            _context = context;
        }

        [HttpGet]
        [ProducesResponseType ((int) HttpStatusCode.NotFound)]
        [ProducesResponseType (typeof (Customer), (int) HttpStatusCode.OK)]
        public ActionResult findAll () {
            var customer = from s in _context.Customers select s;
            return Ok (customer.ToList ());
        }

        [HttpPost, Route ("create")]
        public ActionResult Create ([FromBody] Customer customer) {
            try {
                if (ModelState.IsValid) {
                    _context.Customers.Add (customer);
                    _context.SaveChanges ();
                    return Ok (new { success = "Customer Registred" });
                } else {
                    return NotFound ();
                }
            } catch (DataException /* dex */ ) {

                ModelState.AddModelError ("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return Ok (customer);
        }

        [HttpPut ("{id}")]
        public IActionResult Put (string id, [FromBody] Customer customer) {
            try {
                if (ModelState.IsValid) {
                    if(customer.id != Convert.ToInt32(id)) {
                        return BadRequest (new { error = "Invalid data"});    
                    }
                    _context.Customers.Update (customer);
                    _context.SaveChanges ();
                    return Ok (new { success = "Customer updated" });
                } else {
                    return NotFound ();
                }
            } catch (DataException /* dex */ ) {
                return BadRequest (new { error = "Unable to save changes. Try again, and if the problem persists see your system administrator."});
            }
        }

        [HttpDelete ("{id}")]
        public IActionResult Delete (string id) {
            try {
                Customer customer = new Customer();
                customer.id = Convert.ToInt32(id);
                _context.Customers.Attach (customer);
                _context.Customers.Remove (customer);
                _context.SaveChanges ();
                return Ok (new { success = "Customer deleted" });
            } catch (DataException) {
                return BadRequest(new { error = "Unable to delete customer. Try again, and if the problem persists see your system administrator." });
            }
        }
    }
}