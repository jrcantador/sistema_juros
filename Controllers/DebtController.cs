using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using db.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sistema.Models;

namespace sistema.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class DebtController : Controller {

        private readonly dbContext _context;

        public DebtController (dbContext context) {
            _context = context;
        }

        [HttpGet ("customer/{customerId}")]
        [ProducesResponseType ((int) HttpStatusCode.NotFound)]
        [ProducesResponseType (typeof (Customer), (int) HttpStatusCode.OK)]
        public ActionResult findByCustomer (string customerId) {

            var debts = _context.Debts
                .Include (debt => debt.customer)
                .Where (consumer => consumer.id.Equals (Convert.ToInt32 (customerId)));
            return Ok (debts.FirstOrDefault ());
        }

        [HttpPost, Route ("create")]
        public ActionResult Create ([FromBody] Debt debt) {
            try {
                if (ModelState.IsValid) {
                    _context.Debts.Add (debt);
                    _context.SaveChanges ();
                    return Ok (new { success = "Debts Registred" });
                } else {
                    return NotFound ();
                }
            } catch (DataException /* dex */ ) {

                ModelState.AddModelError ("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return Ok (debt);
        }

    }
}