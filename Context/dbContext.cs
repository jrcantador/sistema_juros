using Microsoft.EntityFrameworkCore;
using sistema.Models;

namespace db.Context {
    public class dbContext : DbContext {

        public dbContext (DbContextOptions<dbContext> opts) : base (opts) { }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            modelBuilder.Entity<Debt> ()
                .HasOne (b => b.customer);
        }

        public DbSet<Debt> Debts { get; set; }

        public DbSet<Customer> Customers { get; set; }

    }
}