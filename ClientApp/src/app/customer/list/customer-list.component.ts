import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { NegociationDTO } from 'src/app/dto/negociationDTO.model';
import { SimulationDTO } from 'src/app/dto/simulationDTO.model';
import { Debt } from 'src/app/debt/debt.model';
import { Customer } from '../customer.model';


@Component({
    selector: 'app-customer-list-component',
    templateUrl: './customer-list.component.html'
})
export class CustomerListComponent {

    customers: Array<any> = [];
    debt: Debt = new Debt();
    negociation: NegociationDTO = new NegociationDTO();
    simulation: SimulationDTO = new SimulationDTO();
    installmentsDate = [];

    constructor(private customerService: CustomerService) { }

    ngOnInit() {
        this.findAll();
    }

    findAll() {
        this.customerService.findAll().subscribe(data => this.customers = data);
    }

    ViewDebt(costumerId: string) {
        this.customerService.findDebtsByCustomer(costumerId)
            .subscribe(data => {
                let debtCostumer: any = data;
                this.debt.value = debtCostumer.value;
                this.debt.dueDate = debtCostumer.dueDate;
                this.debt.customer = debtCostumer.customer;
                this.debt.installments = debtCostumer.dueDate;
            });
    }

    simulate() {
        let today: Date = new Date();        
        let valueDebit = this.debt.value;
        let dueDate: Date = new Date(this.debt.dueDate);
        let rate = (this.negociation.valueRate / 100);
        let interest = this.negociation.interest;
        let commission = this.negociation.commission;
        let installments = this.negociation.installments;

        let daysRate = Math.ceil(Math.abs((today.getTime() - dueDate.getTime()) / (1000 * 3600 * 24))) - 1;
        let valueRate: number = 0.00;
        let valueDebtRate: number = 0.00;
        let valueComission: number = 0.00;
        let valueInstallments: number = 0.00;
        if (interest == 1) {
            this.calcularJurosSimples(valueDebit, rate, daysRate, commission, installments);
        } else {
            this.calculaJurosComposto(valueDebit, rate, daysRate, commission, installments);
        }
        this.simulation.simulateDate = today;

        
        for(let i = 0; i < installments; i++){             
            this.installmentsDate[i] = new Date();
            this.installmentsDate[i].setDate(today.getDate() + (i * 30))
        }

    }

    calcularJurosSimples(valueDebit: number, rate: number, daysRate: number, commission: number, installments: number) {
        var valueRate = valueDebit * (1 * rate * daysRate);
        var valueDebtRate = valueRate + valueDebit;
        var valueComission = valueDebtRate * (commission / 100);
        var valueInstallments = valueDebtRate / installments;

        this.simulation.valueRate = valueRate;
        this.simulation.daysRate = daysRate;
        this.simulation.valueDebit = valueDebit;
        this.simulation.valueDebtRate = valueDebtRate;
        this.simulation.valueComission = valueComission;
        this.simulation.valueInstallments = valueInstallments;
    }

    calculaJurosComposto(valueDebit: number, rate: number, daysRate: number, commission: number, installments: number) {
        var rate = rate;
        var valueDebtRate = valueDebit * Math.pow((1 + rate), daysRate);
        var valueRate = valueDebtRate - valueDebit;
        var valueInstallments = valueDebtRate / installments;
        var valueComission = valueDebtRate * (commission / 100);

        this.simulation.valueRate = valueRate;
        this.simulation.daysRate = daysRate;
        this.simulation.valueDebit = valueDebit;
        this.simulation.valueDebtRate = valueDebtRate;
        this.simulation.valueComission = valueComission;
        this.simulation.valueInstallments = valueInstallments;
    }
}

