import { Customer } from "../customer/customer.model";

export class Debt {
  customer: Customer
  dueDate: Date
  installments: number
  value: number
}