export class SimulationDTO {
  simulateDate: Date
  daysRate: number
  valueDebit: number
  valueRate: number
  valueDebtRate: number
  valueComission: number
  valueInstallments: number
}