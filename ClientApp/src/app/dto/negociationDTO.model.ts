export class NegociationDTO {
  installments: number
  interest: number
  valueRate: number
  time: number
  commission: number
}